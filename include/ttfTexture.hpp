#ifndef TTFTEXTURE_HPP
#define TTFTEXTURE_HPP
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <string>
#include <memory>

#include "texture.hpp"

class ttfTexture : public texture {
  public:
    ttfTexture();
    ~ttfTexture();

    statusCode init(std::shared_ptr<SDL_Renderer> renderer,
        std::shared_ptr<TTF_Font> font);
    statusCode load_string(std::string s, SDL_Color textColor);

  private:
    std::shared_ptr<TTF_Font> mFont;
    std::string mTextureText;

};
#endif

