#ifndef IMGTEXTURE_HPP
#define IMGTEXTURE_HPP
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <string>
#include <memory>

#include "texture.hpp"

class imgTexture : public texture {
  public:
    imgTexture();
    ~imgTexture();
    int get_origWidth()  const {return mOrigWidth;};
    int get_origHeight() const {return mOrigHeight;};

    virtual statusCode load_from_file(std::string f);
    virtual statusCode create_blank( int w, int h, SDL_TextureAccess access);
    int mOrigWidth, mOrigHeight;//width/height of source

};
#endif
