#ifndef UTIL_HPP
#define UTIL_HPP
#include <string>
#include <cstdio>
#include <iostream>
#include <memory>

#define CHECK(val,...) check_status( val, __FUNCTION__, ##__VA_ARGS__)
#define DEBUG std::cout << "DEBUG: " << __FUNCTION__
//#define M_PI 3.141592653589793238462643383279502884

enum statusCode  {kSuccess, kFail, NSTATUS};

template<typename ... Args>
statusCode check_status( bool status, const std::string& func,
    const std::string& format, Args ... args) {
  if (status) return kSuccess;
  const std::string fullfmt = func + ": " + format + "\n";
  //following code based on SO answer from iFreilicht, CCO 1.0
  size_t size = snprintf(nullptr, 0, fullfmt.c_str(), args ...) + 1;
  if (size <= 0){throw std::runtime_error( "Error with error string output." );}
  printf(fullfmt.c_str(), args ...);
  return kFail;
}
#endif
