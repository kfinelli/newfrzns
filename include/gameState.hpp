#ifndef GAMESTATE_HPP
#define GAMESTATE_HPP
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <memory>

#include "util.hpp"

class gameState {
  public:
    gameState(std::shared_ptr<SDL_Renderer> renderer,
            std::shared_ptr<SDL_Window> window,
            std::shared_ptr<TTF_Font> font);
    virtual statusCode handle_events() = 0;
    virtual statusCode logic() = 0;
    virtual statusCode render() = 0;

    virtual bool exit() const = 0;
    virtual bool change_state() const = 0;
    virtual std::unique_ptr<gameState> make_new_state()= 0;
  protected:
    std::shared_ptr<SDL_Renderer> mRenderer;
    std::shared_ptr<SDL_Window> mWindow;
    std::shared_ptr<TTF_Font> mFont;

};
#endif
