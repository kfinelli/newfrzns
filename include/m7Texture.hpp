#ifndef M7TEXTURE_HPP
#define M7TEXTURE_HPP
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <memory>
#include <vector>

#include "imgTexture.hpp"

class m7Texture : public imgTexture {
  public:
    m7Texture();
    ~m7Texture();
    statusCode load_from_file(std::string path);
    virtual statusCode render(int x, int y,
        const std::unique_ptr<SDL_Rect> &clip,
        const std::unique_ptr<SDL_Point> &center,
        const SDL_RendererFlip &flip) const;

    void setFocus(double f){mFocus=f;};
    void setCHeight(double hc) {mCHeight=hc;};
    void setXCenter(int x){mAxisX=x;};
    void updateTilt() {mUpdateTilt=true;};
    double getFocus(){return mFocus;};
    double getCHeight() {return mCHeight;};

  protected:
    double mFocus; //distance to viewing screen in `pixels'
    double mCHeight; //camer height in `pixels'
    int mAxisX; //x position
    mutable bool mUpdateTilt;
};
#endif
