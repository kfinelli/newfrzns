#ifndef GAMESTATETITLE_HPP
#define GAMESTATETITLE_HPP
#include <vector>

#include <gameState.hpp>
#include <imgTexture.hpp>
#include <ttfTexture.hpp>

class gameStateTitle : public gameState {
  public:
    gameStateTitle(std::shared_ptr<SDL_Renderer> renderer,
       std::shared_ptr<SDL_Window> window,
       std::shared_ptr<TTF_Font> font);
    ~gameStateTitle();
    statusCode handle_events();
    statusCode logic();
    statusCode render();

    bool exit() const {return mExit;};
    bool change_state() const;
    std::unique_ptr<gameState> make_new_state();
  private:
    const unsigned int NUM_MENU_ITEMS = 2;
    std::vector<std::string> menuStrings;
    bool mInputAccept, mExit;
    unsigned int mCursorPos;

    std::unique_ptr<imgTexture> mLogo, mCursor, mBackground;
    std::vector<std::unique_ptr<ttfTexture> > mMenuEntries;
};
#endif
