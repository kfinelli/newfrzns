#ifndef GAMESTATEM7MAP_HPP
#define GAMESTATEM7MAP_HPP
#include <vector>

#include <gameState.hpp>
#include <imgTexture.hpp>
#include <ttfTexture.hpp>
#include <m7Texture.hpp>

class gameStateM7Map : public gameState {
  public:
    gameStateM7Map(std::shared_ptr<SDL_Renderer> renderer,
       std::shared_ptr<SDL_Window> window,
       std::shared_ptr<TTF_Font> font);
    ~gameStateM7Map();
    statusCode handle_events();
    statusCode logic();
    statusCode render();

    bool exit() const {return mExit;};
    bool change_state() const;
    std::unique_ptr<gameState> make_new_state();
  private:
    bool mInputAccept, mExit, mUpdateDebug;
    std::vector<std::string> mDebugStrings;
    unsigned int mCursorPos;
    double mVeloLinear, mVeloAngular;
    double mFocusDelta, mVeloCHeight;
    double mXpos, mYpos;
    double mAngle;
    double mFocus, mCHeight;
    const int LINEAR_SPEED = 5;
    const double FOCUS_STEP=1, CH_STEP=1.0, ANGULAR_SPEED = 1.5;

    std::unique_ptr<imgTexture> mSkybox, mCharacter, mMapView;
    std::unique_ptr<m7Texture> mMap;
    std::vector<std::unique_ptr<ttfTexture> > mDebugText;
};
#endif
