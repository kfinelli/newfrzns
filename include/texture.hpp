#ifndef TEXTURE_HPP
#define TEXTURE_HPP
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <string>
#include <memory>

#include "util.hpp"

class texture {
  public:
    texture();
    virtual ~texture();

    virtual statusCode init(std::shared_ptr<SDL_Renderer>);
    virtual statusCode render(int x, int y,
        const std::unique_ptr<SDL_Rect> &clip) const;
    statusCode render(int x, int y) const
    {return render(x,y, nullptr);};
    virtual statusCode render(int x, int y,
        const std::unique_ptr<SDL_Rect> &clip,
        const std::unique_ptr<SDL_Point> &center,
        const SDL_RendererFlip &flip) const;
    virtual statusCode create_blank( int w, int h, SDL_TextureAccess access);
    statusCode set_color(uint8_t r, uint8_t g, uint8_t b);
    statusCode set_alpha(uint8_t a);
    statusCode set_blend_mode(SDL_BlendMode b);
    statusCode reset();
    void setWidthHeight(int w, int h) {mWidth=w;mHeight=h;};
    void setWindow(std::shared_ptr<SDL_Window> window) {mWindow = window;};
    statusCode set_as_render_target() const;
    void set_angle(double a) {mAngle=a;};

    int get_width() const {return mWidth;};
    int get_height() const {return mHeight;};

  protected:
    std::unique_ptr<SDL_Texture, decltype(&SDL_DestroyTexture)> mTexture;
    std::shared_ptr<SDL_Renderer> mRenderer;
    std::shared_ptr<SDL_Window> mWindow;
    int mWidth, mHeight;//width/height to draw
    double mAngle;
};
#endif
