TARGET := newfrzns
INCDIRS := include
SRCDIR := src
OBJDIR := obj
DEPDIR := $(OBJDIR)/.deps

SRCS := $(wildcard $(SRCDIR)/*.cpp)
OBJS := $(SRCS:%.cpp=$(OBJDIR)/%.o)
DEPFLAGS = -MT $@ -MMD -MP -MF $(DEPDIR)/$*.d
LDLIBS := -lSDL2 -lSDL2_image -lSDL2_ttf

INCFLAGS := $(addprefix -I,$(INCDIRS))
CXXFLAGS := $(INCFLAGS) -std=c++14 -Wall -pedantic -g
COMPILE.cpp = $(CXX) $(DEPFLAGS) $(CFLAGS) $(CXXFLAGS) $(TARGET_ARCH) -c

$(OBJDIR)/%.o : %.cpp $(DEPDIR)/%.d
	@mkdir -p '$(@D)'
	@mkdir -p $(^D)
	$(COMPILE.cpp) $(OUTPUT_OPTION) $<

$(TARGET) : $(OBJS)
	$(CXX) $(LDFLAGS)$^ $(LDLIBS) -o $@

#$(DEPDIR): ; @mkdir -p $@

DEPFILES := $(SRCS:%.cpp=$(DEPDIR)/%.d)
$(DEPFILES):
include $(wildcard $(DEPFILES))
