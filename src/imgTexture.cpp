#include "imgTexture.hpp"

imgTexture::imgTexture() :
  texture() { }

imgTexture::~imgTexture() {}

statusCode imgTexture::load_from_file(std::string path) {
  reset();

  //Load image at specified path
  std::unique_ptr<SDL_Surface, decltype(&SDL_FreeSurface)>
    loadedSurface(IMG_Load( path.c_str() ), SDL_FreeSurface);

  if (CHECK(loadedSurface != nullptr, "Unable to load image %s,"
        "SDL Error: %s\n", path.c_str(), IMG_GetError()) == statusCode::kFail) {
    return statusCode::kFail;
  }

  //Color key image
  SDL_SetColorKey( loadedSurface.get(), SDL_TRUE, SDL_MapRGB( loadedSurface->format, 0, 0xFF, 0xFF ) );

  //Create texture from surface pixels
  mTexture.reset(SDL_CreateTextureFromSurface( mRenderer.get(), loadedSurface.get() ));

  if(CHECK(mTexture != nullptr, "Unable to create texture from %s,"
        "SDL Error: %s\n", path.c_str(), SDL_GetError()) == statusCode::kFail) {
    return statusCode::kFail;
  }

  //Get image dimensions
  mOrigWidth = loadedSurface->w;
  mOrigHeight = loadedSurface->h;
  mWidth = loadedSurface->w;
  mHeight = loadedSurface->h;

  //Return success
  return statusCode::kSuccess;
}

statusCode imgTexture::create_blank( int w, int h, SDL_TextureAccess access) {
  texture::create_blank(w, h, access);
  mOrigWidth=w;
  mOrigHeight=h;
  return kSuccess;
}

