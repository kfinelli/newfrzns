#include <gameStateTitle.hpp>
#include <gameStateM7Map.hpp>

gameStateTitle::gameStateTitle(std::shared_ptr<SDL_Renderer> renderer,
    std::shared_ptr<SDL_Window> window,
    std::shared_ptr<TTF_Font> font ) :
  gameState(renderer, window, font),
  mInputAccept(false),
  mExit(false),
  mCursorPos(0),
  mLogo(new imgTexture()),
  mCursor(new imgTexture()),
  mBackground(new imgTexture()),
  mMenuEntries()
{
  //load menu strings
  //TODO: make resources file for strings, write loader class
  menuStrings = {"Start", "Quit"};

  mLogo->init(mRenderer);
  mLogo->load_from_file("resources/title.png");
  mCursor->init(mRenderer);
  mCursor->load_from_file("resources/cursor.png");
  mBackground->init(mRenderer);
  mBackground->load_from_file("resources/bg_rect.png");
  //TODO: load color from resource file?
  SDL_Color textColor = { 229, 229, 0 };
  for (unsigned i = 0; i < NUM_MENU_ITEMS; ++i) {
    mMenuEntries.push_back(std::make_unique<ttfTexture>());
    mMenuEntries.back()->init(mRenderer, mFont);
    mMenuEntries.back()->load_string(menuStrings.at(i), textColor);
  }
}

statusCode gameStateTitle::handle_events() {
  SDL_Event e;
  while (SDL_PollEvent(&e) ) {
    if (e.type == SDL_KEYDOWN) {
      switch( e.key.keysym.sym ) {
        case SDLK_UP:
          if (mCursorPos > 0) mCursorPos--;
          break;
        case SDLK_DOWN:
          if (mCursorPos < NUM_MENU_ITEMS-1) mCursorPos++;
          break;
        case SDLK_SPACE:
          mInputAccept = true;
          break;
      }
    } else if (e.type == SDL_QUIT) {
      mExit = true;
    }
  }
  return kSuccess;
}

statusCode gameStateTitle::logic() {
  return kSuccess;
}

statusCode gameStateTitle::render() {
  SDL_RenderClear( mRenderer.get() );
  int w=0, h=0;
  SDL_GetRendererOutputSize( mRenderer.get(), &w, &h);
  mBackground->setWidthHeight(w,h);
  statusCode sc = mBackground->render(0, 0);
  sc = (statusCode)(mLogo->render(120, 80) | sc);
  sc = (statusCode)(mCursor->render(190, 350 + 40 * mCursorPos) | sc);
  for (unsigned i = 0; i < NUM_MENU_ITEMS; ++i) {
    sc = (statusCode)(mMenuEntries.at(i)->render(260, 360 + 40 * i) | sc);
  }

  SDL_RenderPresent( mRenderer.get() );
  return sc;
}

bool gameStateTitle::change_state() const {
  return mInputAccept;
}

std::unique_ptr<gameState> gameStateTitle::make_new_state() {
  switch (mCursorPos) {
    case 0:
      return std::unique_ptr<gameState>(
          new gameStateM7Map(mRenderer, mWindow, mFont));
      break;
    case 1:
      mExit = true;
      break;
    default:
      throw std::runtime_error( "Error: bad cursor state on title screen" );
  }
  return nullptr;
}
