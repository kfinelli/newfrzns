#include <gameState.hpp>

gameState::gameState(std::shared_ptr<SDL_Renderer> renderer,
    std::shared_ptr<SDL_Window> window,
    std::shared_ptr<TTF_Font> font) :
  mRenderer(renderer), mWindow(window),
  mFont(font) {}
