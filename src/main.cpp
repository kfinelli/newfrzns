#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <memory>

#include "gameState.hpp"
#include "gameStateTitle.hpp"
#include "texture.hpp"
#include "util.hpp"

//Screen dimension constants
const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 960;

int main() {
  //SDL Initializations
  CHECK( SDL_Init(SDL_INIT_VIDEO) == 0,
      "Failed to init SDL, SDL Error: %s\n", SDL_GetError());
  CHECK( SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1") == SDL_TRUE,
      "Failed to set hint, SDL Error: %s\n", SDL_GetError());

  //Shared SDL objects: window and renderer
  std::shared_ptr<SDL_Window> gWindow(SDL_CreateWindow( "newfrzns",
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        SCREEN_WIDTH,
        SCREEN_HEIGHT,
        //TODO use fullscreen by default?
        SDL_WINDOW_SHOWN ), SDL_DestroyWindow);
  CHECK(gWindow != nullptr,
      "Failed to create window, SDL Error: %s\n", SDL_GetError());

  std::shared_ptr<SDL_Renderer> gRenderer(
      SDL_CreateRenderer( gWindow.get(), -1,
        SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC ),
      SDL_DestroyRenderer);
  CHECK(gRenderer != nullptr,
      "Failed to create renderer, SDL Error: %s\n", SDL_GetError());

  SDL_SetRenderDrawColor(gRenderer.get(), 0xFF, 0xFF, 0xFF, 0xFF);

  CHECK(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG,
      "Failed to init SDL IMG, IMG Error: %s\n", IMG_GetError());

  CHECK(TTF_Init() == 0,
      "Failed to init SDL TTF, TTF Error: %s\n", TTF_GetError());

  //load fonts (only one for now)
  //TODO #9 : create font services wrapper class
  std::shared_ptr<TTF_Font> gFont(TTF_OpenFont(
        "/usr/share/fonts/TTF/DejaVuSans.ttf", 28 ),
      TTF_CloseFont); 
  if (gFont==nullptr) {
    gFont.reset(TTF_OpenFont("/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf",
          28 ), TTF_CloseFont);
  }
  CHECK(gFont != nullptr,
      "Failed to load font, TTF Error: %s\n", TTF_GetError());

  //move all of the below to game state manager singleton
  //initial game state is title screen
  std::unique_ptr<gameState> currentState(new gameStateTitle(gRenderer, gWindow, gFont));


  //main game loop
  statusCode mainStatus = kSuccess;
  while (currentState->exit()==false && mainStatus==kSuccess) {

    mainStatus = CHECK(currentState->handle_events()==kSuccess,
        "Failure in handle_events() state machine");

    mainStatus = CHECK(currentState->logic()==kSuccess,
        "Failure in logic() state machine");

    if (currentState->change_state()) {
      currentState = currentState->make_new_state();
      if (currentState==nullptr) break;
    }

    mainStatus = CHECK(currentState->render()==kSuccess,
        "Failure in render() state machine");

  }

  currentState.reset(nullptr);
  //SDL quit functions
  gWindow = nullptr;
  gRenderer = nullptr;
  TTF_Quit();
  IMG_Quit();
  SDL_Quit();

  return 0;
}
