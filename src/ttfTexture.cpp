#include "ttfTexture.hpp"

ttfTexture::ttfTexture() :
  texture(),
  mTextureText() { }

ttfTexture::~ttfTexture() {}

statusCode ttfTexture::init(std::shared_ptr<SDL_Renderer> renderer,
    std::shared_ptr<TTF_Font> font) {
  mFont = font;
  return texture::init(renderer);
}

statusCode ttfTexture::load_string(std::string s, SDL_Color textColor) {
  reset();

  //Load image at specified path
  std::unique_ptr<SDL_Surface, decltype(&SDL_FreeSurface)>
    loadedSurface(TTF_RenderText_Solid(mFont.get(), s.c_str(), textColor),
        SDL_FreeSurface);

  if (CHECK(loadedSurface != nullptr, "Unable to load ttf texture, "
        "SDL Error: %s\n", TTF_GetError()) == statusCode::kFail) {
    return statusCode::kFail;
  }

  //Color key image
  SDL_SetColorKey( loadedSurface.get(), SDL_TRUE, SDL_MapRGB( loadedSurface->format, 0, 0xFF, 0xFF ) );

  //Create texture from surface pixels
  mTexture.reset(SDL_CreateTextureFromSurface( mRenderer.get(), loadedSurface.get() ));

  if(CHECK(mTexture != nullptr, "Unable to create ttf texture, "
        "SDL Error: %s\n", SDL_GetError()) == statusCode::kFail) {
    return statusCode::kFail;
  }

  //Get image dimensions
  mWidth = loadedSurface->w;
  mHeight = loadedSurface->h;

  //Return success
  return statusCode::kSuccess;
}


