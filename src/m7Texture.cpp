#include "m7Texture.hpp"

m7Texture::m7Texture() :
  imgTexture() { }

m7Texture::~m7Texture() {}

statusCode m7Texture::load_from_file(std::string path) {
  //re-define this method to specify streaming texture
  reset();

  //Load image at specified path
  std::unique_ptr<SDL_Surface, decltype(&SDL_FreeSurface)>
    loadedSurface(IMG_Load( path.c_str() ), SDL_FreeSurface);

  if (CHECK(loadedSurface != nullptr, "Unable to load image %s,"
        "SDL Error: %s\n", path.c_str(), IMG_GetError()) == statusCode::kFail) {
    return statusCode::kFail;
  }

  //convert surface to display format
  std::unique_ptr<SDL_Surface, decltype(&SDL_FreeSurface)>
    formattedSurface(SDL_ConvertSurfaceFormat(loadedSurface.get(),
          SDL_GetWindowPixelFormat(mWindow.get()), 0), SDL_FreeSurface);

  if (CHECK(formattedSurface != nullptr, "Unable to format image, "
        "SDL Error: %s\n", IMG_GetError()) == statusCode::kFail) {
    return statusCode::kFail;
  }

  //Color key image
  SDL_SetColorKey( formattedSurface.get(),
      SDL_TRUE, SDL_MapRGB( formattedSurface->format, 0, 0xFF, 0xFF ) );

  //Create texture from surface pixels
  mTexture.reset(SDL_CreateTexture( mRenderer.get(),
        SDL_GetWindowPixelFormat(mWindow.get()),
        SDL_TEXTUREACCESS_STREAMING, formattedSurface->w, formattedSurface->h));
  if(CHECK(mTexture != nullptr, "Unable to create texture from %s,"
        "SDL Error: %s\n", path.c_str(), SDL_GetError()) == statusCode::kFail) {
    return statusCode::kFail;
  }
  void *vpixels=nullptr;
  int pitch=0;
  SDL_LockTexture(mTexture.get(), nullptr, &vpixels, &pitch);

  memcpy(vpixels, formattedSurface->pixels,
      formattedSurface->pitch * formattedSurface->h);
  //Uint32* pixels = (Uint32*)vpixels;
  SDL_UnlockTexture(mTexture.get());

  //Get image dimensions
  mOrigWidth = loadedSurface->w;
  mOrigHeight = loadedSurface->h;

  //Return success
  return statusCode::kSuccess;
}

statusCode m7Texture::render(int x, int y,
    const std::unique_ptr<SDL_Rect> &clip,
    const std::unique_ptr<SDL_Point> &center,
    const SDL_RendererFlip &flip) const {
  //render the texture to screen at (x,y) with clipping rectangle clip
  SDL_Rect renderQuad = { x, y, mWidth, mHeight };
  statusCode sc = statusCode::kSuccess;

  //debug render
  //if (true) {
  //  SDL_Rect clipRect = {0, 0, mOrigWidth, mHeight};
  //  sc = (statusCode) (SDL_RenderCopy( mRenderer.get(), mTexture.get(), &clipRect,
  //        &renderQuad) | sc);
  //  return sc;
  //}
  //end debug render

  //DEBUG <<" mOrigHeight: " << mOrigHeight
  //  << " mOrigWidth: " << mOrigWidth << std::endl;;

  for (int j=0;j<mHeight;j++) {
    int newwidth = mOrigWidth/2*((j+1)/mCHeight);

    int new_ycoord = ((int)(mOrigHeight-mFocus*mCHeight/(j+1)))%mOrigHeight;
    if (new_ycoord < 0) new_ycoord = mOrigHeight+new_ycoord;
    SDL_Rect clipRect = {0, new_ycoord, mOrigWidth, 1};

    int num_x_draws = newwidth>0 ? (mOrigWidth/newwidth+2): 3;
    for (int k = 0; k<num_x_draws; ++k){
      int x_offset = k%2 ? k/2+1: -k/2;
      SDL_Rect drawRect = {renderQuad.x+mWidth/2-newwidth/2+x_offset*newwidth, renderQuad.y+j,
        newwidth, 1};

      sc = (statusCode) (SDL_RenderCopy( mRenderer.get(), mTexture.get(), &clipRect,
            &drawRect) | sc);
    } //end loop over x coord
  } //end loop over y coord
  mUpdateTilt=false;
  return sc;
}
