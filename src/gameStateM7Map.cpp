#include <gameStateM7Map.hpp>
#include <cmath>

gameStateM7Map::gameStateM7Map(std::shared_ptr<SDL_Renderer> renderer,
    std::shared_ptr<SDL_Window> window,
    std::shared_ptr<TTF_Font> font ) :
  gameState(renderer, window, font),
  mExit(false),
  mVeloLinear(0), mVeloAngular(0),
  mFocusDelta(0), mVeloCHeight(0),
  mXpos(800), mYpos(1200), mAngle(90),
  mFocus(200), mCHeight(90.0),
  mSkybox(new imgTexture()),
  mMapView(new imgTexture()),
  mMap(new m7Texture())
{
  int w=0, h=0;
  SDL_GetRendererOutputSize(mRenderer.get(), &w, &h);
  mMapView->init(mRenderer);
//  mMapView->load_from_file("resources/000bSecretOfManaOverworldMap.png");
//  mMapView->load_from_file("resources/SuperMarioKartMapMushroomCup1.png");
  mMapView->load_from_file("resources/rainbow_checks.png"); //(1600x1200)
  mSkybox->init(mRenderer);
  mSkybox->load_from_file("resources/sky.png");
  mMap->init(mRenderer);
  mMap->create_blank(mMapView->get_width()*1.5, mMapView->get_height()*1.5,
      SDL_TEXTUREACCESS_TARGET);
  mMap->setWindow(mWindow);
  mMap->setCHeight(mCHeight);
  mMap->setFocus(mFocus);
  //mMap->setXCenter(w/2);
  //mMap->updateTilt();
  mUpdateDebug=true;


  mDebugStrings = {"PosX: %g", "PosY: %g", "Angle: %g", "CHeight: %g", "Focus: %g"};
  SDL_Color textColor = { 0, 0, 0 };
  for (unsigned i = 0; i < /*NUM_DEBUG_ITEMS*/5; ++i) {
    mDebugText.push_back(std::make_unique<ttfTexture>());
    mDebugText.back()->init(mRenderer, mFont);
    mDebugText.back()->load_string(mDebugStrings.at(i), textColor);
  }
}

statusCode gameStateM7Map::handle_events() {
  SDL_Event e;
  while (SDL_PollEvent(&e) ) {
    //map, handleEvent e (to be factored)
    if (e.type == SDL_KEYDOWN && e.key.repeat == 0) {
      switch( e.key.keysym.sym ) {
        case SDLK_UP:
          mVeloLinear += LINEAR_SPEED;
          break;
        case SDLK_DOWN:
          mVeloLinear -= LINEAR_SPEED;
          break;
        case SDLK_LEFT:
          mVeloAngular += ANGULAR_SPEED;
          break;
        case SDLK_RIGHT:
          mVeloAngular -= ANGULAR_SPEED;
          break;
        case SDLK_q:
          mFocusDelta += FOCUS_STEP;
          break;
        case SDLK_a:
          mFocusDelta -= FOCUS_STEP;
          break;
        case SDLK_w:
          mVeloCHeight += CH_STEP;
          break;
        case SDLK_s:
          mVeloCHeight -= CH_STEP;
          break;
      }
      mUpdateDebug=true;
      //mMap->updateTilt();
    } else if (e.type == SDL_KEYUP && e.key.repeat == 0) {
      switch( e.key.keysym.sym ) {
        case SDLK_UP:
          mVeloLinear -= LINEAR_SPEED;
          break;
        case SDLK_DOWN:
          mVeloLinear += LINEAR_SPEED;
          break;
        case SDLK_LEFT:
          mVeloAngular -= ANGULAR_SPEED;
          break;
        case SDLK_RIGHT:
          mVeloAngular += ANGULAR_SPEED;
          break;
        case SDLK_q:
          mFocusDelta -= FOCUS_STEP;
          break;
        case SDLK_a:
          mFocusDelta += FOCUS_STEP;
          break;
        case SDLK_w:
          mVeloCHeight -= CH_STEP;
          break;
        case SDLK_s:
          mVeloCHeight += CH_STEP;
          break;
      }
      mUpdateDebug=true;
      //mMap->updateTilt();
    } else if (e.type == SDL_QUIT) {
      mExit = true;
    }
  }
  return kSuccess;
}

statusCode gameStateM7Map::logic() {
  mAngle += mVeloAngular;
  mXpos += ((double)mVeloLinear * std::cos(2*M_PI*((double)mAngle)/360.));
  mYpos -= ((double)mVeloLinear * std::sin(2*M_PI*((double)mAngle)/360.)); //y-coord increases downard
  if (mAngle > 360) mAngle = mAngle - 360;
  if (mAngle < 0) mAngle = mAngle + 360;
  mFocus += mFocusDelta;
  mCHeight += mVeloCHeight;
  mMapView->set_angle(-90+mAngle); //rotate ccw, no rotation = 90
  mMap->setFocus(mFocus);
  mMap->setCHeight(mCHeight);

  return kSuccess;
}

statusCode gameStateM7Map::render() {
  int w=0, h=0;
  SDL_GetRendererOutputSize(mRenderer.get(), &w, &h);
  mSkybox->setWidthHeight(w,h);

  statusCode sc = kSuccess;
  SDL_RenderClear(mRenderer.get());
  sc = (statusCode)(mSkybox->render(0, 0) | sc);

  std::unique_ptr<SDL_Point> mapview_center(
      new SDL_Point({
//        (int)(mXpos + mFocus * std::cos(2*M_PI*((double)mAngle)/360.)),
//        (int)(mYpos - mFocus * std::sin(2*M_PI*((double)mAngle)/360.)) }));
        (int)(mXpos),
        (int)(mYpos) }));
//        mMapView->get_width()/2,
//        mMapView->get_height()/2}));

  mMap->set_as_render_target();
  SDL_SetRenderDrawColor(mRenderer.get(), 0x00, 0x90, 0x00, 0xFF);
  SDL_RenderClear( mRenderer.get());

  //DEBUG<< mMap->get_height() <<std::endl;

  sc = (statusCode)(mMapView->render(
        -mXpos + mMap->get_origWidth() - w,
        -mYpos + mMap->get_origHeight(),
        nullptr, mapview_center, SDL_FLIP_NONE) | sc);

  SDL_SetRenderTarget(mRenderer.get(), nullptr);

  mMap->setWidthHeight(w,h/2);
  sc = (statusCode)(mMap->render(0, h/2,
        nullptr, nullptr, SDL_FLIP_NONE) | sc);


//debug text
  for (unsigned i = 0; i < /*NUM_MENU_ITEMS*/5; ++i) {
    if (mUpdateDebug) {
      double value = 0;
      switch (i) {
        case 0: value = mXpos; break;
        case 1: value = mYpos; break;
        case 2: value = mAngle; break;
        case 3: value = mMap->getCHeight(); break;
        case 4: value = mMap->getFocus(); break;
      }
      char buf[50];
      sprintf(buf, mDebugStrings.at(i).data(), value);
      std::string s(buf);
    mDebugText.at(i)->load_string(buf, SDL_Color({255,255,255}));
    }
    sc = (statusCode)(mDebugText.at(i)->render(60, 60 + 40 * i) | sc);
  }
  mUpdateDebug=false;


  SDL_RenderPresent(mRenderer.get() );

  return sc;
}

bool gameStateM7Map::change_state() const {
  //stub
  return false;
}

std::unique_ptr<gameState> gameStateM7Map::make_new_state() {
  //stub
  return nullptr;
}
