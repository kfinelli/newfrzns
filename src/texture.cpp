#include "texture.hpp"

texture::texture() :
  mTexture(nullptr, SDL_DestroyTexture), 
  mRenderer(nullptr),
  mWidth(0), mHeight(0),
  mAngle(0) { }

texture::~texture() {}

statusCode texture::reset() {
  mTexture = nullptr;
  mWidth = mHeight = 0;
  return statusCode::kSuccess;
}

statusCode texture::init(std::shared_ptr<SDL_Renderer> renderer) {
  mRenderer = renderer;
  return statusCode::kSuccess;
}

statusCode texture::render(int x, int y,
    const std::unique_ptr<SDL_Rect> &clip) const {
  //render the texture to screen at (x,y) with clipping rectangle clip
  SDL_Rect renderQuad = { x, y, mWidth, mHeight };

  //if( clip != nullptr ) {
  //  renderQuad.w = clip->w;
  //  renderQuad.h = clip->h;
  //}

  //Render to screen
  return CHECK(SDL_RenderCopy( mRenderer.get(), mTexture.get(), clip.get(),
        &renderQuad)==0,
      "texture::render failed, SDL Error %s", SDL_GetError() );
}

statusCode texture::render(int x, int y,
    const std::unique_ptr<SDL_Rect> &clip,
    const std::unique_ptr<SDL_Point> &center,
    const SDL_RendererFlip &flip) const {
  //render the texture to screen at (x,y) with clipping rectangle clip
  SDL_Rect renderQuad = { x, y, mWidth, mHeight };

  //if( clip != nullptr ) {
  //  renderQuad.w = clip->w;
  //  renderQuad.h = clip->h;
  //}

  //Render to screen
  return CHECK(SDL_RenderCopyEx( mRenderer.get(), mTexture.get(), clip.get(),
        &renderQuad,
        mAngle, center.get(), flip)==0,
      "texture::render failed, SDL Error %s", SDL_GetError() );

}

statusCode texture::create_blank( int w, int h, SDL_TextureAccess access) {
  //create mTexture as blank (pixelformat rgba8888 hardcoded)
  mTexture.reset(SDL_CreateTexture(mRenderer.get(), SDL_PIXELFORMAT_RGBA8888,
        access, w, h));
  if(CHECK(mTexture != nullptr, "Unable to create blank texture "
        "SDL Error: %s\n", SDL_GetError()) == statusCode::kFail) {
    return statusCode::kFail;
  }
  mWidth=w;
  mHeight=h;
  return kSuccess;
}

statusCode texture::set_color(Uint8 red, Uint8 green, Uint8 blue) {
  //Modulate texture rgb
  return CHECK(SDL_SetTextureColorMod(mTexture.get(), red, green, blue)==0,
      "texture::set_color failed, SDL Error %s\n", SDL_GetError() );
}

statusCode texture::set_blend_mode(SDL_BlendMode b) {
  //Set blending function
  return CHECK(SDL_SetTextureBlendMode(mTexture.get(), b) == 0,
      "texture::set_blend_mode failed, SDL Error %s\n", SDL_GetError() );
}

statusCode texture::set_alpha(Uint8 alpha) {
  //Modulate texture alpha
  return CHECK(SDL_SetTextureAlphaMod(mTexture.get(), alpha)==0,
      "texture::set_alpha failed, SDL Error %s\n", SDL_GetError() );
}

statusCode texture::set_as_render_target() const {
  //Use this texture as a surface for next render
  return CHECK(SDL_SetRenderTarget( mRenderer.get(), mTexture.get())==0,
      "texture::set_as_render_target failed, SDL ERROR %s\n",
      SDL_GetError() );
}
